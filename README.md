# IDONE

IDONE uses a piece-wise linear surrogate model for optimization of expensive cost functions with integer variables. The method is described in [this paper](https://doi.org/10.1007/s10472-020-09712-4).

# Citation

Please cite our paper if you use this code:


```
@article{bliek2020black,
  title={Black-box combinatorial optimization using models with integer-valued minima},
  author={Bliek, Laurens and Verwer, Sicco and de Weerdt, Mathijs},
  journal={Annals of Mathematics and Artificial Intelligence},
  pages={1--15},
  year={2020},
  publisher={Springer}
}
```

# Description

`IDONE_minimize(f, x0, lb, ub, max_evals)` solves the minimization problem

**min** *f(x)*

**st.** *lb<=x<=ub, x is integer*

where `f` is the objective function, `x0` the initial guess,
`lb` and `ub` are the bounds (assumed integer), 
and `max_evals` is the maximum number of objective evaluations.

It is the discrete version of the [DONE algorithm](https://bitbucket.org/csi-dcsc/donecpp/src/master/).

This version runs in Python 3.7.

## Warning
IDONE is known to suffer from low exploration. The MVRSM algorithm is known to give better performance, even on discrete problems. See the [MVRSM github page](https://github.com/lbliek/MVRSM) or the [EXPObench library](https://github.com/AlgTUDelft/ExpensiveOptimBenchmark).

# Dependencies

* Numpy (tested on version 1.17)
* Scipy (tested on version 1.3)

A demo file to run IDONE on a higher-dimensional Rosenbrock function has been included. To run it, run `demo_Rosenbrock.py`.

# Contact 
Please contact l.bliek@tue.nl if you have any questions.






